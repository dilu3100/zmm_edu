import time
from celery import Celery
from kombu import Queue


app = Celery('tiger_2')

app.conf.worker_concurrency = 1

# app.conf.broker_url = 'redis://:password@redis_db:6379/0'
app.conf.broker_url = 'redis://:password@localhost:6380/0'

app.conf.task_queues = [
    Queue('tiger_2'),
]

app.conf.task_routes = {
    'dance': {
        'queue': 'tiger_1'
    }
}


@app.task(name='dance')
def dance(tiger_1_said):
    script = {
        '只': '老',
        '虎': '爱',
        '跳': '舞',
        '哎': '小',
        '兔': '子',
        '乖乖': '拔',
        '萝': '卜'
    }

    if tiger_1_said == '诶':
        return

    i_say = script[tiger_1_said]
    print(f'[Tiger 2] ♫ 😼: {i_say}')
    time.sleep(0.5)
    app.send_task('dance', [i_say])
