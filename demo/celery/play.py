from celery import Celery

app = Celery('tiger_2')
# app.conf.broker_url = 'redis://:password@redis_db:6379/0'
app.conf.broker_url = 'redis://:password@localhost:6380/0'
app.send_task('dance', args=['两'], queue='tiger_1')
