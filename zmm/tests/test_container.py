from django.urls import reverse
from rest_framework import status

from . import ZMMTestCase


class ContainerTestCase(ZMMTestCase.use_role('admin')):

    def get_view_url(self, detail=False):
        if detail:
            return reverse('containers-detail')
        else:
            return reverse('containers')

    def test_create_valid_shipping_container(self):
        TEST_DATA = {
            "name": "test-container-1",
            "height": 200.0,
            "width": 1165.0,
            "length": 1165.0
        }

        response = self.client.post(
            self.get_view_url(),
            data=TEST_DATA,
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictContainsSubset(TEST_DATA, response.json())
