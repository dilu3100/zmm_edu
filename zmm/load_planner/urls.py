from django.urls import path
from . import views


urlpatterns = [
    path('shipping-containers/', views.ShippingContainerViewSet.as_view({
        'get': 'list',
        'post': 'create'
    }), name='containers'),

    path('shipping-containers/<str:pk>/', views.ShippingContainerViewSet.as_view({
        'delete': 'destroy',
        'patch': 'partial_update',
    }), name='containers-detail'),

    path('tasks/', views.TaskViewSet.as_view({
        'get': 'list',
        'post': 'create',
    }), name='tasks'),

    path('tasks/<str:pk>/', views.TaskViewSet.as_view({
        'patch': 'partial_update',
        'delete': 'destroy',
        'get': 'retrieve',
    }), name='tasks-detail')
]
