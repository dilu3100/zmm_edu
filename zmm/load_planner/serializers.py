from rest_framework import serializers
from common.models import User
from .models import ShippingContainer, Task


class ShippingContainerSerializer(serializers.ModelSerializer):

    class Meta:
        model = ShippingContainer
        fields = '__all__'


class TaskSerializer(serializers.ModelSerializer):

    creator = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    class Meta:
        model = Task
        fields = '__all__'
        read_only_fields = ['status', 'result_dataset', 'submitted_at', 'created_at', 'finished_at']
        extra_kwargs = {
            'config': {'required': False},
            'input_dataset': {'required': False},
        }
