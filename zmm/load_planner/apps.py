from django.apps import AppConfig


class LoadPlannerConfig(AppConfig):
    name = 'load_planner'
    scoped = True
