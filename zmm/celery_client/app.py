import os
from celery import Celery
from kombu import Queue


_broker_addr = os.environ['REDIS_HOST']
_port = os.environ['REDIS_PORT']
_password = os.environ['REDIS_PASSWORD']


app = Celery('celery_client')

app.conf.worker_concurrency = 1

app.conf.broker_url = f'redis://:{_password}@{_broker_addr}:{_port}/0'

app.conf.task_queues = [
    Queue('zmm.results.queue'),
    Queue('zmm.internal.queue'),
]

app.conf.task_routes = {
    'zmm.*': {
        'queue': 'zmm.request.queue'
    },
    'internal.*': {
        'queue': 'zmm.internal.queue'
    }
}
