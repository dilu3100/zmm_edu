from django.urls import path
from . import views


urlpatterns = [
    path('login/', views.LoginView.as_view()),
    path('register/', views.RegisterView.as_view()),
    path('users/', views.UserViewSet.as_view({
        'get': 'list',
        'patch': 'partial_update',
    })),
    path('users/<str:pk>/', views.UserViewSet.as_view({
        'patch': 'partial_update',
    })),
]
