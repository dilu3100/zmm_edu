from django.db import transaction
from django.db import connection
from django.core.management.base import CommandParser
from django.core.management.commands import migrate

from ...models import Tenant
from ...utils.db import schema_exists, create_schema


class Command(migrate.Command):

    def _apply_migrate(self, only_public: bool, *args, **options):
        all_schemas = ['public']

        if not only_public:
            # Migrate all schemas
            all_schemas += list(Tenant.objects.values_list('schema_name', flat=True))

        # Do it one by one
        for schema in all_schemas:
            print(f'> 正在同步 {schema}')

            # Ensure schema exists
            if not schema_exists(schema):
                print(f'{schema}不存在，创建中')
                create_schema(schema)

            # Enable schema access
            connection.set_schema(schema, include_public=schema != 'public')

            # Apply migrations
            super().handle(*args, **options)
            # migrate.Command().execute(*args, **options)

            try:
                transaction.commit()
                connection.close()
                connection.connection = None
            except transaction.TransactionManagementError:
                # Probably inside an atomic transaction so no need to commit & close connection now
                pass

    def add_arguments(self, parser: CommandParser):
        super().add_arguments(parser)
        parser.add_argument('--public', choices=['yes', 'no'], default='no')

    def handle(self, *args, **options):
        only_public = options['public'].lower().strip() == 'yes'
        self._apply_migrate(only_public, *args, **options)
