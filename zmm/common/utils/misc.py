import datetime
from rest_framework_simplejwt.tokens import RefreshToken


def issue_token_for(user, lifetime=None):
    """
    Generate a refresh token and access token pair having the date
    of issue information.
    """
    refresh_token = RefreshToken.for_user(user)
    access_token = refresh_token.access_token

    if lifetime is not None:
        access_token.set_exp(lifetime=lifetime)

    return {
        'refresh': str(refresh_token),
        'access': str(access_token)
    }
