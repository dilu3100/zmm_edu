from types import new_class
from rest_framework.permissions import BasePermission, DjangoModelPermissions


class CustomDjangoModelPermissions(DjangoModelPermissions):
    perms_map = {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': [],
        'HEAD': [],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }


class IsAdmin(BasePermission):

    admin_only_methods = []

    @classmethod
    def guard_methods(cls, *methods):
        new_perm_cls = new_class('IsAdminWithControlledMethods', (cls,))
        new_perm_cls.admin_only_methods = list(map(str.lower, methods))
        return new_perm_cls

    def _is_action_allowed(self, method, user):
        if method.lower() in self.admin_only_methods:
            return getattr(user, 'is_admin', False)
        return True

    def has_permission(self, request, view):
        return self._is_action_allowed(request.method, request.user)

    def has_object_permission(self, request, view, obj):
        return self._is_action_allowed(request.method, request.user)


class CanModifyUser(BasePermission):
    """
    由于所有的用户都在public Schema，我们需要更加精细地控制哪些数据可以被操作
    """
    def has_object_permission(self, request, view, obj):
        user = request.user

        # Cannot touch user in the other tenant
        if user.tenant != obj.tenant:
            return False

        # Admin can control users in the same tenant
        if user.is_admin:
            return True

        # Basic user can only update own instance
        return user.id == obj.id
