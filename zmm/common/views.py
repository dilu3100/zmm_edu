import os
from django.core.management import call_command
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework import status, response, generics, viewsets

from .models import Role, User, Tenant
from .serializers import UserSerializer, RegistrationSerializer
from .utils.misc import issue_token_for
from .permissions import CustomDjangoModelPermissions, CanModifyUser


class LoginView(TokenObtainPairView):

    def post(self, request, *args, **kwargs):
        email, password = request.data.get('email'), request.data.get('password')

        serializer = self.get_serializer(data={
            'username': User.make_default_username(email),
            'password': password
        })

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])

        instance = serializer.user
        user_data = UserSerializer(instance).data
        tokens = serializer.validated_data

        return response.Response({
            'tokens': tokens,
            'user': user_data
        }, status=status.HTTP_200_OK)


class RegisterView(generics.GenericAPIView):
    # 注册接口是公开的，不需要做身份验证
    permission_classes = ()
    serializer_class = RegistrationSerializer

    def post(self, request):
        data = request.data
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)

        # Create a tenant for the user
        schema_name = User.make_default_tenant_schema(data['email'])
        tenant = Tenant.objects.create(
            schema_name=schema_name,
            organization=data.get('organization'))

        # Migrate models to the tenant schema
        call_command('tenant_migrate')

        # Create "basic" and "admin" roles for the tenant
        default_role = os.environ['DJANGO_DEFAULT_USER_ROLE']

        if default_role == 'admin':
            user_role = Role.objects.create_admin_role(tenant)
        elif default_role == 'basic':
            user_role = Role.objects.create_basic_role(tenant)
        else:
            raise ValueError(f'Unknown default role name {default_role}')

        # Create the new user under the tenant, with "admin" role
        user = User.objects.create(
            email=data['email'],
            password=data['password'],
            first_name=data['first_name'],
            last_name=data['last_name'],
            tenant=tenant,
            roles=[user_role]
        )
        serializer = UserSerializer(user)
        user_token = issue_token_for(user)

        return response.Response({
            'user': serializer.data,
            'access': user_token['access']
        }, status=status.HTTP_200_OK)


class UserViewSet(viewsets.ModelViewSet):

    permission_classes = [
        CustomDjangoModelPermissions,
        CanModifyUser
    ]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_object(self):
        """
        Depends on whether a specifci lookup PK is given, will
        retrieve the current user's or the corresponding user's instance.
        """
        if not self.kwargs:
            user_id = self.request.user.id
            filter_kwargs = {self.lookup_field: user_id}
        else:
            filter_kwargs = {self.lookup_field: self.kwargs[self.lookup_field]}

        queryset = self.filter_queryset(self.get_queryset())
        instance = generics.get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, instance)

        return instance

    def list(self, request, *args, **kwargs):
        user_id = self.request.user.id
        instance = User.objects.get(id=user_id)
        data = UserSerializer(instance).data
        return response.Response(data, status=status.HTTP_200_OK)
