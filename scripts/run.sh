./scripts/postgres_wait.sh

# Start UI app
python -m ui.index &

# Start Compute Celery app
celery -A compute worker -l INFO &

# Maybe reset database
cd zmm

if [ "$DJANGO_RESET_DB" = true ]; then
    echo 'Completely resetting database... all previous data will be dropped!'
    python manage.py reset_db
fi

# Ensure Django apps are migrated
python manage.py tenant_migrate --public yes

# Migrate ZMM apps
python manage.py makemigrations common
python manage.py makemigrations load_planner
python manage.py tenant_migrate

# Start ZMM API apps
celery -A celery_client worker -l INFO &
python manage.py runserver 0.0.0.0:8080
