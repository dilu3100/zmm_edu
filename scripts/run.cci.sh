# Start Database and Redis
service postgresql start
service redis-server start

# Check database connection
./scripts/postgres_wait.sh


# Ensure Django apps are migrated
cd zmm
python manage.py tenant_migrate --public yes

# Migrate ZMM apps
python manage.py makemigrations common
python manage.py makemigrations load_planner
python manage.py tenant_migrate

# Start ZMM API apps
python manage.py runserver 0.0.0.0:8080 &
celery -A celery_client worker -l INFO &

# Start ZMM Compute apps
cd ..
celery -A compute worker -l INFO &

# Start ZMM UI app
python -m ui.index
