# FROM python:3.8
FROM registry.dorabot.com/pub/dr_docker/python:3.8
# FROM swr.cn-east-2.myhuaweicloud.com/zmm-edu/python:3.8


RUN mkdir /code
COPY . /code
WORKDIR /code

RUN pip config set global.index-url http://mirrors.aliyun.com/pypi/simple/ && \
    pip config set global.trusted-host mirrors.aliyun.com && \
    pip install -r requirements.txt
