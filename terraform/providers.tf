terraform {
  required_providers {
    huaweicloud = {
      source  = "huaweicloud/huaweicloud"
      version = ">= 1.25"
    }
  }

  required_version = ">= 0.15.0"
}


provider "huaweicloud" {
  region  = "cn-east-2"
#   access_key = "..."
#   secret_key = "..."
}
