terraform {
  backend "remote" {
    hostname      = "app.terraform.io"
    organization  = "sxz"

    workspaces {
      prefix = "zmm-edu-"
    }
  }
}