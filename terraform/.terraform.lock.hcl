# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/huaweicloud/huaweicloud" {
  version     = "1.25.0"
  constraints = "~> 1.25"
  hashes = [
    "h1:IrBM3TkZZPhp78str0/GOKG+rZeusITVnt5GNutdlIo=",
    "zh:076eb6c7f646c5669054d63ab930862573a1e7c4c31f25dd972a64719ca8e33b",
    "zh:0aa9f0b1c9a525f73414e28b238e79cdd41ac0481ecdb0b266059786a85b8593",
    "zh:14f6123b511ddada6462daf05fc97f9ca8d6a0c3ff04eebfdfb202b803498ad5",
    "zh:1d4bee457cdda6f8156bdd64d9fe50b31e3443f48b18544e8046595ff8af0d68",
    "zh:2bc2057c289fe4ca3a575d0d2261008687ca1ddbd521dce7cd98fc77ce4ecaff",
    "zh:2c805d520d1664490fa1726144b79c663a0f0390ff57939ac9922bd18858fd30",
    "zh:4903b4f465e1ef82f103876d6379e0953f417ae0e474379bc9611bbb8bb1d797",
    "zh:5fc0f142b6b7d9fbbc08eeec183098ecef9455866954333969ce5c67641c0811",
    "zh:6f5fd42d5e0c90ba6f20880e89954ee9015a1df22c68f17b2d4ecc1fb53a8f9e",
    "zh:704e9bfe0b73fca7e55e17f6c7240dbebfd1587c27e2655a2672c4f6b2f8fef0",
    "zh:75ae4f31478355ec9da551bd2f7c51ff6fb23da43fed053273efa7bfbe90a05d",
    "zh:864661cfdb91b87c0da8191fcde2d31e6d0ef0a390632d1e0867bfbcb96d8a27",
  ]
}
