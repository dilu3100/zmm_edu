data "huaweicloud_vpc" "vpc" {
  name = "${var.project_name}-edu"
}

data "huaweicloud_vpc_subnet" "subnet" {
  name = "${var.project_name}-edu-subnet"
}
