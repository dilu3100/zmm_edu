## ZMM SaaS Education Version

### [1] Create containerized Django project
0. Create .gitignore
1. Create Python3.8 environmen
2. Install required packages.
3. Init the project, let's call it "zmm": `django-admin startproject zmm`
4. Create a new app "load_planner": `python manage.py startapp load_planner`:
    - This is where all ZMM Container Loading features are implemented
5. Create a new app "common": `python manage.py startapp common`
    - This app takes care of common SaaS features, like user system and data segregation
6. Create `.env` file
7. Update `settings.py` to implement those changes:
    - Add `ENVIRONMENT`, read from env var
    - Update `DATABASES` to use PostgreSQL database
    - Add `common` and `load_planner` to `INSTALLED_APPS`
8. In `scripts` folder, copy `postgres_wait.sh` to it and write new `run.sh`.
9. Crete Dockerfile and docker-compose yaml
10. Try it: `docker-compose up`


### [2] Implement Database Segregation
0. Create `common.utils.db`: add a helper function to decide whether a schema exists
1. Create `common.psql_backend` module
3. Create `common.utils.routers`.
    - Because the router reads Django app `is_zmm_app` attribute to decide the nature of current app, this step also includes add the attribute to `common` and `load_planner` app classes.
4. Create `tenant_migrate` Django command
    - Because migration is applied *per tenant*, this step also involves creating the `Tenant` model.
5. Update `settings.py`


### [3] Login and Register APIs
0. Create `User` and `Role` model.
1. Set `AUTH_USER_MODEL` to `common.User` in `settings.py`
2. Implement `/register` view.
    - By default, the user role is `admin`
    - Implement manager classes for User and Role along the way
3. Implement `/login` view.
    - Need to add `djangorestframework_simplejwt` to project requirements
    - Need to add `rest_framework_simplejwt` to INSTALLED_APPS
    - Use the existing `TokenObtainPairView` which automatically does token validation.

### [4] Implement RBAC
**A: Prepare basic data**

0. Create `ShippingContainer` model, serializer and viewset:
    - So that we have basic model on which various access controls can be experimented.
    - Demonstrated as a typical usage of DRF ModelSerializer and ModelViewSet
2. Add `shipping-containers` urls
3. Re-up docker-compose to apply migrations 
    - Then calling GET will cause ProgrammingError saying relation `load_planner_shippingcontainer` does not exist. This is expected because the model exists in the tenant schema, not the public schema.
4. Copy `middleware.py` code to `zmm/common/middleware.py`
5. Update `settings.py`:
    - Set `DEFAULT_AUTHENTICATION_CLASSES` of DRF settings to `rest_framework_simplejwt.authentication.JWTAuthentication`
    - Append `common.middleware.TenantMiddleware` to the MIDDLEARE setting.
6. GET container with bearer token. Verify it returns an empty list


**B: Implement custom authentication backend**

0. Add `IsAuthenticated` and `CustomDjangoModelPermissions` to DRF default permission classes.
1. Verify that this time, even with the correct user token, we will receive 403 error. That's because Django doesn't know how to lookup user permission from his Role, which is defined on our own.
2. Implement `RoleAuthenticationBackend` in `authentication.py`, and add it to `AUTHENTICATION_BACKENDS` in settings
3. GET containers again. This time it returns an empty list as expected
4. To see the working difference, let's change the default user role to "basic", who is only able to view Container:
    - Add `DJANGO_DEFAULT_USER_ROLE` to .env
    - Implement `RoleManager.create_basic_role`.
5. Reset DB and register a new user but this time with "basic" role.
6. Verify that when POST container, will receive 403 response saying insufficient permission.


### [5] Compute Service

**A: Lay down the foundation**

0. Implement `Task` model according to ERD
1. Update `RoleManager.create_basic_role` to add `Task` permissions to the role.
2. Create `Task` serializer and viewset, similar to what's done to the `Container` stuff.
3. Create Celery client at API side.
4. Implement Celery stub tasks. Notice that each task handler requires database access, hence the helper decorator that sets database access scope before invoking the handler.
5. Create Celery client at Compute side
6. Implement Celery stub tasks. Here we fake the algorithm behaviour which is supposed to give the `result_dataset`.


**B: Implement functions**

0. Implement API's Celery tasks details. Remember to lock the affected task.
1. Implement `/tasks` PATCH:
    - Once PATCH gives `{"submit": true}`, immediately submits the task for computation.
    - `tenant_id` can be read from the request user object


### Misc Note:
* Planner input protocol: https://gitlab.dorabot.com/aiot/loulan_cad_app/dr_loading_loulan_protocol/-/blob/master/template/loulan/plan/plan_readme.md


### TODO:
[ ] Prepare the bare minimal base image in case it takes too long for students to build images locally.


