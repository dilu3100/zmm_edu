import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output

from .. import api_client, components
from ..app import app


def _on_container_table_init():
    return api_client.list_container()


def _on_task_table_init():
    return api_client.list_task()


def _on_container_table_edit(how):
    def handler(state, data, _):
        sync_db = state == 'idle'
        print(f'Sync DB = {sync_db}')

        if not sync_db:
            return data

        if how == 'delete':
            api_client.destroy_container(data['id'])

        if how == 'update':
            data = api_client.update_container(data['id'], data)

        if how == 'insert':
            data = api_client.create_container(data)

        return data
    return handler


def _on_task_table_edit(how):
    def handler(state, data, _):
        sync_db = state == 'idle'

        if not sync_db:
            return data

        if how == 'delete':
            api_client.destroy_task(data['id'])

        if how == 'update':
            data = api_client.update_task(data['id'], data)

        if how == 'insert':
            user_id = api_client.read_token_body()['user_id']
            data['creator'] = user_id
            data = api_client.create_task(data)

        return data
    return handler


def _get_task_detail_link(data):
    return f'/tasks/{data["id"]}'


content = dbc.Container([
    html.Br(),
    html.H3('Welcome', id='home-welcome-header'),
    html.Br(),
    html.H5('Tasks'),
    components.datatable.new(
        app=app,
        id_prefix='tasks',
        columns=[
            {'name': 'Name', 'id': 'name', 'editable': True},
            {'name': 'Status', 'id': 'status', 'editable': False},
            {'name': 'Create Time', 'id': 'created_at', 'editable': False},
        ],
        row_deletable=True,
        on_init=_on_task_table_init,
        on_update=_on_task_table_edit('update'),
        on_insert=_on_task_table_edit('insert'),
        on_delete=_on_task_table_edit('delete'),
        inspect_link_getter=_get_task_detail_link
    ),
    html.Br(),
    html.H5('Shipping Containers'),
    components.datatable.new(
        app=app,
        id_prefix='containers',
        columns=[
            {'name': 'Name', 'id': 'name', 'editable': True},
            {'name': 'Length', 'id': 'length', 'editable': True},
            {'name': 'Width', 'id': 'width', 'editable': True},
            {'name': 'Height', 'id': 'height', 'editable': True},
            {'name': 'Create Time', 'id': 'created', 'editable': False},
        ],
        row_deletable=True,
        on_init=_on_container_table_init,
        on_update=_on_container_table_edit('update'),
        on_insert=_on_container_table_edit('insert'),
        on_delete=_on_container_table_edit('delete'),
    ),
], id='home-page')


# Callbacks
# ---------
@app.callback(
    Output('home-welcome-header', 'children'),
    Input('home-page', 'children'))
def on_landed(_):
    user_data = api_client.get_user()
    welcome_text = f'Welcome, {user_data["first_name"]} {user_data["last_name"]} ({user_data["email"]})'
    return welcome_text
