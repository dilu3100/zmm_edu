import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
from ..api_client import is_user_authenticated


def new(app, id, **kwargs):
    content = dbc.NavbarSimple(
        id=id,
        children=[],
        brand='装满满SaaS 教学版',
        brand_href='/home',
        color='primary', 
        dark=True,
        **kwargs)

    app.callback(
        Output(id, 'children'),
        Input('page-content', 'children')
    )(_on_content_refresh)

    return content


def _on_content_refresh(__):
    if not is_user_authenticated():
        return [
            dbc.NavItem(dbc.NavLink('Login', href='/login')),
            dbc.NavItem(dbc.NavLink('Sign Up', href='/register'))
        ]

    return [
        dbc.NavItem(dbc.NavLink('Home', href='/home')),
        dbc.NavItem(dbc.NavLink('Profile', href='/profile'))
    ]
