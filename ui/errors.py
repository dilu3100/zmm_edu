class UnauthorizedError(Exception):
    pass


class BadRequestError(Exception):
    pass


class ServerError(Exception):
    pass


class UnknownAPIClientError(Exception):
    pass
